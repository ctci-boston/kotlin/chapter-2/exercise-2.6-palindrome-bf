import com.pajato.ctci.Node
import com.pajato.ctci.createLinkedList
import kotlin.test.Test
import kotlin.test.assertEquals

class Test {

    @Test fun `verify an empty list returns true`() {
        assertEquals(true, isPalindrome(null))
    }

    @Test fun `verify a single character list returns true`() {
        assertEquals(true, isPalindrome(Node('a'.toInt())))
    }

    @Test fun `verify a simple non-palindrome returns false`() {
        assertEquals(false, isPalindrome(createLinkedList('a'.toInt(), 'b'.toInt())))
    }

    @Test fun `verify a complex non-palindrome returns false`() {
        assertEquals(false, isPalindrome("thisIsNotAPalindrome".convertToLinkedList()))
    }

    @Test fun `verify a complex palindrome returns true`() {
        assertEquals(true, isPalindrome("thisIsAPalindromeemordnilaPAsIsiht".convertToLinkedList()))
    }

    // Extension functions.

    fun String.convertToLinkedList(): Node? {
        val charArray = this.toCharArray()
        val head = if (this.isBlank()) null else Node(charArray[0].toInt())
        tailrec fun buildList(current: Node, index: Int): Node? {
            if (index >= charArray.size) return head
            val next = Node(charArray[index].toInt())
            current.next = next
            return buildList(next, index + 1)
        }

        return if (head == null) null else buildList(head, 1)
    }
}
