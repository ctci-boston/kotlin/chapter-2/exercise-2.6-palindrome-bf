import com.pajato.ctci.Node

fun isPalindrome(node: Node?): Boolean {
    fun getArray(): Array<Char> {
        val result = Array(node?.length() ?: 0) { '0' }
        tailrec fun convertToArray(current: Node?, index: Int): Array<Char> {
            if (current == null) return result
            result[index] = current.value.toChar()
            return convertToArray(current.next, index + 1)
        }

        return node?.let { convertToArray(node, 0) } ?: Array(0) { '0' }
    }
    tailrec fun isPalindrome(array: Array<Char>, lower: Int, upper: Int): Boolean {
        if (lower >= upper) return true
        if (array[lower] != array[upper]) return false
        return isPalindrome(array, lower + 1, upper - 1)
    }

    val array = getArray()
    return node?.let { isPalindrome(array, 0, array.size - 1) } ?: true
}
