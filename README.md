# Exercise 2.6 Palindrome

Implement a function to check if a linked list is a palindrome.

_Hints_: #5, #13, #29, #61, #101

Whiteboard [analysis and test cases](whiteboard-images/analysis.png) as well as [code](whiteboard-images/code.png) are available via the links.

Questions, issues, comments, PRs all welcome.
